package pe.interbank.maverick.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAutoConfiguration
@ComponentScan
@SpringBootApplication
@EnableDiscoveryClient
@EnableAsync
@ComponentScan({ "pe.interbank.maverick" })
public class ConfigurationApplication {
	public static void main(String[] args) {
		SpringApplication.run(ConfigurationApplication.class, args);
	}

}
