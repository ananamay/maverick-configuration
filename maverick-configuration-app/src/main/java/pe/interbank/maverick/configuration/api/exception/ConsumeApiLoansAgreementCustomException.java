package pe.interbank.maverick.configuration.api.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.commons.core.exception.BaseException;
import pe.interbank.maverick.commons.core.exception.ErrorDto;
import pe.interbank.maverick.configuration.core.util.ErrorType;




@Getter
@Setter
public class ConsumeApiLoansAgreementCustomException extends BaseException{

	private final ErrorDto error;
	
	private static final long serialVersionUID = 1L;
	
	public ConsumeApiLoansAgreementCustomException(ErrorType errorType) {
		super(errorType.getCode(), errorType.getTitle(), errorType.getUserMessage(), errorType.getSystemMessage(), errorType.getHttpStatus());
		error = super.getError();
	}
	
	public ConsumeApiLoansAgreementCustomException(String code,String title, String userMessage, String systemMessage, HttpStatus httpStatus) {
		super(code, title, userMessage, systemMessage, httpStatus);
		error = super.getError();
	}
	
	public ConsumeApiLoansAgreementCustomException(ErrorType errorType, String systemMessage) {
		super(errorType.getCode(), errorType.getTitle(), errorType.getUserMessage(), systemMessage, errorType.getHttpStatus());
		error = super.getError();
	}
	
}
