package pe.interbank.maverick.configuration.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import pe.interbank.maverick.commons.core.exception.GenericErrorDto;
import pe.interbank.maverick.configuration.core.util.ErrorType;


@ControllerAdvice(basePackages = "pe.interbank")
@RestController
public class CustomExceptionHandler extends RestResponseEntityExceptionHandler {

	@Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
        HttpHeaders headers, HttpStatus status, WebRequest request) {
      CustomException customException = new CustomException(ErrorType.INVALID_REQUEST);
      GenericErrorDto genericErrorDto = new GenericErrorDto();
      genericErrorDto.setError(customException.getError());
      return new ResponseEntity<>(genericErrorDto, customException.getError().getHttpStatus());
    }

}
