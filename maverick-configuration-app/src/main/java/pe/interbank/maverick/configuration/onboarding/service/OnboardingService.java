package pe.interbank.maverick.configuration.onboarding.service;

import pe.interbank.maverick.configuration.parameters.onboarding.dto.OnboardingRequestDto;
import pe.interbank.maverick.configuration.parameters.onboarding.dto.OnboardingResponseDto;

public interface OnboardingService {

	OnboardingResponseDto getParameters(OnboardingRequestDto parametersRequestDto);
}
