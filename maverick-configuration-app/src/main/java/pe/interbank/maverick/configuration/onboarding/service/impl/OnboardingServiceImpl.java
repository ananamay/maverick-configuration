package pe.interbank.maverick.configuration.onboarding.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.interbank.maverick.commons.core.model.user.Application;
import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.configuration.onboarding.service.OnboardingService;
import pe.interbank.maverick.configuration.parameters.onboarding.dto.OnboardingRequestDto;
import pe.interbank.maverick.configuration.parameters.onboarding.dto.OnboardingResponseDto;
import pe.interbank.maverick.configuration.repository.user.UserRepository;
import pe.interbank.maverick.configuration.utility.ConfigurationUtilily;

@Service
public class OnboardingServiceImpl implements OnboardingService {

	@Autowired
	ConfigurationUtilily configurationUtilily;

	@Autowired
	UserRepository userRepository;

	@Override
	public OnboardingResponseDto getParameters(OnboardingRequestDto onboardingRequestDto) {
		OnboardingResponseDto onboardingResponseDto = new OnboardingResponseDto();
		onboardingResponseDto.setShow(getStatusOnboardingCustomer(onboardingRequestDto));
		return onboardingResponseDto;
	}

	private boolean getStatusOnboardingCustomer(OnboardingRequestDto onboardingRequestDto) {
		boolean viewOnboarding = false;
		
		User user = configurationUtilily.getUser(onboardingRequestDto.getUserId());
		if (null != user && null == user.getApplication()) {
			Application application = new Application();
			application.setVersion(onboardingRequestDto.getApplicationVersion());
			user.setApplication(application);
			userRepository.save(user);
			viewOnboarding = true;
		}else if(null != user && null != user.getApplication() && null!= user.getApplication().getVersion()) {
			String[] applicationVersion = onboardingRequestDto.getApplicationVersion().split("\\.");
			String[] applicationVersionInMongo = user.getApplication().getVersion().split("\\.");
			int currentVersion = Integer.parseInt(applicationVersion[0].concat(applicationVersion[1]));
			int versionInMongo = Integer.parseInt(applicationVersionInMongo[0].concat(applicationVersionInMongo[1]));
			if(versionInMongo != currentVersion) {
				Application application = new Application();
				application.setVersion(onboardingRequestDto.getApplicationVersion());
				user.setApplication(application);
				viewOnboarding = true;
				userRepository.save(user);
			}
		}
		
		return viewOnboarding;
	}

}
