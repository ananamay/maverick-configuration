package pe.interbank.maverick.configuration.parameters.controller.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.interbank.maverick.commons.core.util.UtilGenericToken;
import pe.interbank.maverick.configuration.parameters.dto.ParametersRequestDto;
import pe.interbank.maverick.configuration.parameters.dto.ParametersResponseDto;
import pe.interbank.maverick.configuration.parameters.service.impl.ParametersServiceImpl;


@RefreshScope
@RestController
@RequestMapping("/private")
public class ParametersController {

	@Autowired
	ParametersServiceImpl parametersServiceImpl;

	@PostMapping(value = "/parameters")
	public ResponseEntity<ParametersResponseDto> getParameters(@RequestHeader("Authorization") String token,
			@RequestHeader("Session-Id") String sessionId, @RequestHeader("X-MESSAGE-ID") String messageId, @RequestBody ParametersRequestDto parametersRequestDto) {
		parametersRequestDto.setUserId(UtilGenericToken.getClaim(token, "user_name"));
		parametersRequestDto.setSessionId(sessionId);
		parametersRequestDto.setMessageId(messageId);
		ParametersResponseDto parametersResponseDto = parametersServiceImpl.getParameters(parametersRequestDto);
		return new ResponseEntity<>(parametersResponseDto, HttpStatus.OK);
	}

}
