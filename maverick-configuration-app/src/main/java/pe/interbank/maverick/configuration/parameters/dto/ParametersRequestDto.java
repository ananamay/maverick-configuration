package pe.interbank.maverick.configuration.parameters.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParametersRequestDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String applicationVersion;
	private String messageId;
	private String sessionId;
	private String userId;

}
