package pe.interbank.maverick.configuration.parameters.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.configuration.parameters.onboarding.dto.OnboardingResponseDto;

@Getter
@Setter
public class ParametersResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private OnboardingResponseDto onboarding;
}
