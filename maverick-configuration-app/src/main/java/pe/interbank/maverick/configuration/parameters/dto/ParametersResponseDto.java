package pe.interbank.maverick.configuration.parameters.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParametersResponseDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private ParametersResponse configurations;
}
