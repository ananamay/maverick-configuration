package pe.interbank.maverick.configuration.parameters.onboarding.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OnboardingRequestDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String applicationVersion;
	private String messageId;
	private String sessionId;
	private String userId;

}
