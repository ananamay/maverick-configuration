package pe.interbank.maverick.configuration.parameters.onboarding.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OnboardingResponseDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private boolean show;
}
