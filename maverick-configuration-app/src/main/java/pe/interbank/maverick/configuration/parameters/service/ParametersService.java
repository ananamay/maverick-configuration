package pe.interbank.maverick.configuration.parameters.service;

import pe.interbank.maverick.configuration.parameters.dto.ParametersRequestDto;
import pe.interbank.maverick.configuration.parameters.dto.ParametersResponseDto;

public interface ParametersService {

	ParametersResponseDto getParameters(ParametersRequestDto parametersRequestDto);
}
