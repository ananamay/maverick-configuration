package pe.interbank.maverick.configuration.parameters.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.interbank.maverick.configuration.onboarding.service.impl.OnboardingServiceImpl;
import pe.interbank.maverick.configuration.parameters.dto.ParametersRequestDto;
import pe.interbank.maverick.configuration.parameters.dto.ParametersResponse;
import pe.interbank.maverick.configuration.parameters.dto.ParametersResponseDto;
import pe.interbank.maverick.configuration.parameters.onboarding.dto.OnboardingRequestDto;
import pe.interbank.maverick.configuration.parameters.service.ParametersService;

@Service
public class ParametersServiceImpl implements ParametersService {

	@Autowired
	OnboardingServiceImpl onboardingServiceImpl;

	@Override
	public ParametersResponseDto getParameters(ParametersRequestDto parametersRequestDto) {
		ParametersResponseDto parametersResponseDto = new ParametersResponseDto();
		ParametersResponse parametersResponse = new ParametersResponse();

		OnboardingRequestDto onboardingRequestDto = setRequestOnboarding(parametersRequestDto);
		parametersResponseDto.setConfigurations(getParametersOnboarding(parametersResponse, onboardingRequestDto));
		return parametersResponseDto;
	}

	private OnboardingRequestDto setRequestOnboarding(ParametersRequestDto parametersRequestDto) {
		OnboardingRequestDto onboardingRequestDto = new OnboardingRequestDto();
		onboardingRequestDto.setApplicationVersion(parametersRequestDto.getApplicationVersion());
		onboardingRequestDto.setMessageId(parametersRequestDto.getMessageId());
		onboardingRequestDto.setSessionId(parametersRequestDto.getSessionId());
		onboardingRequestDto.setUserId(parametersRequestDto.getUserId());
		return onboardingRequestDto;
	}

	private ParametersResponse getParametersOnboarding(ParametersResponse parametersResponse,
			OnboardingRequestDto onboardingRequestDto) {
		parametersResponse.setOnboarding(onboardingServiceImpl.getParameters(onboardingRequestDto));
		return parametersResponse;
	}

}
