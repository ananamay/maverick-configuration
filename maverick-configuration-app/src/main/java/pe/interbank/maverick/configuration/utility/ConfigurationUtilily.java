package pe.interbank.maverick.configuration.utility;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.configuration.core.util.ErrorType;
import pe.interbank.maverick.configuration.core.util.SensitiveData;
import pe.interbank.maverick.configuration.exception.CustomException;
import pe.interbank.maverick.configuration.repository.user.UserRepository;

@Component
public class ConfigurationUtilily {

	@Autowired
	UserRepository userRepository;

	@Autowired
	SensitiveData sensitiveData;

	public String getDni(String userId) {
		Optional<User> userFound = userRepository.findById(userId);
		if (userFound.isPresent()) {
			return sensitiveData.decrypt(userFound.get().getDni());
		} else {
			throw new CustomException(ErrorType.ERROR_UNREGISTERED_USER);
		}

	}

	public ConsumeApiRequestDto setRequestApi(String messageId, String userId, String sessionId) {
		ConsumeApiRequestDto consumeApiRequestDto = new ConsumeApiRequestDto();
		consumeApiRequestDto.setMessageId(messageId);
		consumeApiRequestDto.setUserId(userId);
		consumeApiRequestDto.setSessionId(sessionId);
		return consumeApiRequestDto;
	}
	
	public User getUser(String userId) {
		Optional<User> userFound = userRepository.findById(userId);
		User user = null;
		if (userFound.isPresent()) {
			user = userFound.get();
		} 
		return user;
	}

}
