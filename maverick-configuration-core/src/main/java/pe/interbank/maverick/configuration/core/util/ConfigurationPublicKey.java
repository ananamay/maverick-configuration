package pe.interbank.maverick.configuration.core.util;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.microsoft.azure.keyvault.KeyVaultClient;
import com.microsoft.azure.keyvault.webkey.JsonWebKey;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Configuration
public class ConfigurationPublicKey {
	

	private static final Logger logger = LoggerFactory.getLogger(ConfigurationPublicKey.class);
	
	@Value("${main.keyvault.keyIdentifierHsm}")
	String keyIdentifierHsm;


	Cipher getCipher(KeyVaultClient keyVaultClient) {
		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance("RSA/ECB/OAEPwithSHA-1andMGF1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, getPublicKeyKeyVault(keyVaultClient));
		} catch (InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
		}
		return cipher;
	}

	
	 @Bean
	 PublicKey getPublicKeyKeyVault(KeyVaultClient keyVaultClient) {
		PublicKey publicKey = null;
		try {
			JsonWebKey jsonWebKey = keyVaultClient.getKeyAsync(keyIdentifierHsm, null).get().key();
			byte[] publicModulus = jsonWebKey.n();
			byte[] publicExponent = jsonWebKey.e();
			publicKey = getPublicKey(publicModulus, publicExponent);
		}catch (Exception e) {
			logger.error(e.getMessage());
		}
		return publicKey;
	}

	private PublicKey getPublicKey(byte[] publicModulus, byte[] publicExponent) {
		BigInteger modulus = new BigInteger(1, publicModulus);
		BigInteger exponent = new BigInteger(1, publicExponent);
		try {
			RSAPublicKeySpec rsaPubKey = new RSAPublicKeySpec(modulus, exponent);
			KeyFactory fact = KeyFactory.getInstance("RSA");
			return fact.generatePublic(rsaPubKey);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

}
