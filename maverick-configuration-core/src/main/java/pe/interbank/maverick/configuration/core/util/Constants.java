package pe.interbank.maverick.configuration.core.util;

public final class Constants {
	private Constants() {
		
	}
    public static final String PREFIX_TOKEN                        = "Bearer ";
    public static final String TAG_USER_NAME                       = "user_name";
    public static final String ZONE                                = "America/Lima";
    public static final String PROVIDER_CODE_NOT_CREDITS           = "0009";
    public static final String PROVIDER_CODE_NOT_DETAILS_CREDIT    = "0010";
    public static final String SPACE                               = " ";
    public static final String CREDIT_CANCELED                     ="Cancelado";
    public static final String CREDIT_DISBURSED                    ="SinDesembolso";
	public static final String EMPTY                               = "";

}
