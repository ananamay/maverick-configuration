package pe.interbank.maverick.configuration.core.util;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public enum ErrorType {
	ERROR_GENERIC(ErrorConstants.CODE_GENERIC_ERROR, ErrorConstants.ERROR, ErrorConstants.GENERIC_ERROR,
			"Error interno", HttpStatus.INTERNAL_SERVER_ERROR),
	NO_CREDITS("01.06.24", "¡Hola! Ya no tienes un Crédito por Convenio con nosotros.", "",
			"El cliente no tiene un credito vigente.", HttpStatus.NOT_FOUND),
	INVALID_REQUEST(ErrorConstants.CODE_GENERIC_ERROR, ErrorConstants.ERROR, ErrorConstants.GENERIC_ERROR,
			"Request no válido", HttpStatus.INTERNAL_SERVER_ERROR),
	NULLPOINTER_REQUEST(ErrorConstants.CODE_GENERIC_ERROR, ErrorConstants.ERROR, ErrorConstants.GENERIC_ERROR,
			"java.lang.NullPointerException", HttpStatus.INTERNAL_SERVER_ERROR),
	ERROR_UNREGISTERED_USER(ErrorConstants.CODE_GENERIC_ERROR, "Usuario no registrado", ErrorConstants.GENERIC_ERROR,
			"El DNI ingresado no existe en la colección.", HttpStatus.NOT_FOUND);

	private String code;
	private String title;
	private String userMessage;
	private String systemMessage;
	private HttpStatus httpStatus;

	ErrorType(String code, String title, String userMessage, String systemMessage, HttpStatus httpStatus) {
		this.code = code;
		this.title = title;
		this.userMessage = userMessage;
		this.systemMessage = systemMessage;
		this.httpStatus = httpStatus;
	}

	private static class ErrorConstants {
		public static final String ERROR = "Error";
		public static final String GENERIC_ERROR = "Ups, ocurrio un error inesperado.";
		public static final String CODE_GENERIC_ERROR = "01.06.99";

	}
}
