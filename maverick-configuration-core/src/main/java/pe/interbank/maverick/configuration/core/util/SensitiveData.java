package pe.interbank.maverick.configuration.core.util;

import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import javax.crypto.Cipher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.microsoft.azure.keyvault.KeyVaultClient;
import com.microsoft.azure.keyvault.webkey.JsonWebKeyEncryptionAlgorithm;
import pe.interbank.maverick.commons.core.azure.KeyVaultConfiguration;
import com.microsoft.azure.keyvault.models.KeyOperationResult;

@Component
public class SensitiveData {

	private static final Logger logger = LoggerFactory.getLogger(SensitiveData.class);
	
	@Autowired
	KeyVaultConfiguration keyVaultConfiguration;
	
	@Autowired
	private KeyVaultClient  keyVaultClient;		

	@Autowired
	PublicKey publicKeyKeyVault;
		
	public String encryptHsm(String sensitiveData) {
		String encryptSensitiveData = null;		
		try {
			final Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPwithSHA-1andMGF1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, publicKeyKeyVault);
			byte[] encryptSensitiveDataBytes = sensitiveData.getBytes(StandardCharsets.UTF_16);
			byte[] cipherData = cipher.doFinal(encryptSensitiveDataBytes);
			encryptSensitiveData = Base64.encodeBase64String(cipherData);
		}catch (Exception e) {
			logger.error(e.getMessage());
		}
		return encryptSensitiveData;
	}	

	public String decrypt(String encryptData) {

		
		String decryptSensitiveData = null;
		byte[] encryptSensitiveDataBytes = Base64.decodeBase64(encryptData);		
		try {
			KeyOperationResult result = keyVaultClient.decrypt(keyVaultConfiguration.getKeyIdentifierHsm(),JsonWebKeyEncryptionAlgorithm.RSA_OAEP, encryptSensitiveDataBytes);
			decryptSensitiveData = new String(result.result(), StandardCharsets.UTF_16);
		} catch (Exception e) {
			decryptSensitiveData = " ";
		}
		return decryptSensitiveData;

	}
	
}
