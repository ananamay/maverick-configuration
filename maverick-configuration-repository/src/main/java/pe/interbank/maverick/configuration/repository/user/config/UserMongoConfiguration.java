package pe.interbank.maverick.configuration.repository.user.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import lombok.Getter;
import pe.interbank.maverick.commons.core.config.MongoConfiguration;

@Configuration
@EnableMongoRepositories(basePackages = "pe.interbank.maverick.configuration.repository.user", mongoTemplateRef = "userMongoTemplate")
@Getter
public class UserMongoConfiguration {


	@Value("${configuration.mongodb.collection.user}")
	String dbCollectionUser;

	@Primary
	@Bean(name = "userMongoTemplate")
	public MongoTemplate userMongoTemplate(MongoConfiguration mongoConfiguration) throws Exception {
		return mongoConfiguration.customMongoTemplate();
	}

}
